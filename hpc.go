package hpc

import (
	"bufio"
	"fmt"
	"io"
	"io/ioutil"
	"os"
	"os/exec"
	"os/user"
	"strconv"
	"strings"
	"syscall"
	"time"

	log "github.com/sirupsen/logrus"

	"github.com/radovskyb/watcher"
)

type Job struct {
	ScriptContents  string
	NativeSpecs     []string
	UID             int
	GID             int
	OutputScriptPth string
	BatchExecution  bool
	PrintToParent   func(string)
	PrintWarning    func(string)
	SpecifiedBatch  string // User specified batch system to be used (automated if nil)
	ParentJobID     string // Parent's unique job id (e.g. CI_JOB_ID)
	CurrentLSF      LSFJob
	CurrentSLURM    SlurmJob
	CurrentCOBALT   CobaltJob
}

type BatchJob interface {
	New() (interface{}, error)
	RunJob() (error, error)
	KillJob() error
}

// timeout for largest possible delay to expect from NFS servers for file data
// This is a Duration string.
const NFSTimeout = "1m"

// printed at the end of log files so we know when to stop tailing.
const JobCompletionString = "Batch job completed successfully."

//This function exists to help the native spec system
func Contains(illegalArgs []string, elementToTest string) bool {
	for _, illegalArg := range illegalArgs {
		if elementToTest == illegalArg {
			return true
		}
		if strings.Contains(elementToTest, illegalArg) {
			return true
		}
	}
	return false
}

// Generate warning (string) if specified arguments are encountered
func (j *Job) CheckIllegalParams(input []string, illegalParams []string) []string {
	var warnings []string
	for i := range input {
		for k := range illegalParams {
			if strings.HasPrefix(input[i], illegalParams[k]) {
				warnings = append(warnings, fmt.Sprint(input[i], " shouldn't be used. It may cause unwanted results"))
			}
		}
	}
	return warnings
}

//Initial func run. Gets batch system and calls the corresponding run
func (j *Job) Run() (builderr error, syserr error) {
	j.updateTargetProfile()
	if !j.BatchExecution {
		return j.RunJob()
	}

	sched, err := j.findScheduler()
	if err != nil {
		return nil, err
	}

	if sched == "slurm" {
		l := new(SlurmJob)
		j.CurrentSLURM, err = l.New(j)
		if err != nil {
			return nil, err
		}
		return j.CurrentSLURM.RunJob()
	} else if sched == "lsf" {
		l := new(LSFJob)
		j.CurrentLSF, err = l.New(j)
		if err != nil {
			return nil, err
		}
		return j.CurrentLSF.RunJob()
	} else if sched == "cobalt" {
		l := new(CobaltJob)
		j.CurrentCOBALT, err = l.New(j)
		if err != nil {
			return nil, err
		}
		return j.CurrentCOBALT.RunJob()
	}

	return nil, fmt.Errorf("No batch system found")
}

func (j *Job) Kill() {
	if !j.BatchExecution {
		return
	}

	sched, _ := j.findScheduler()
	if sched == "slurm" {
		_ = j.CurrentSLURM.KillJob()
	} else if sched == "lsf" {
		_ = j.CurrentLSF.KillJob()
	} else if sched == "cobalt" {
		_ = j.CurrentCOBALT.KillJob()
	}
}

// updateTargetProfile ensures the job script loads the appropriate
// profiles based upon user/admin settings. TODO: expand logic to better
// address user level variables
func (j *Job) updateTargetProfile() {
	script := `#!/bin/bash

[ -f /etc/profile ] && { . /etc/profile || :; }
for f in ~/.bash_profile ~/.bash_login ~/.profile ; do
	if [ -f "$f" ] ; then
		. "$f" || :
		break
	fi
done
unset f

`
	j.ScriptContents = script + j.ScriptContents
}

// buildScript creates a script container the j.ScriptContents and
// returns the absolute path. System error returned if encountered.
func (j *Job) buildScript() (string, error) {
	uniqueID := strconv.FormatInt(time.Now().UnixNano()/int64(time.Millisecond), 10)
	if err := os.MkdirAll(fmt.Sprint(j.OutputScriptPth, "/scripts"), 0740); err != nil {
		return "", err
	}
	if err := os.Chown(fmt.Sprint(j.OutputScriptPth, "/scripts"), j.UID, j.GID); err != nil {
		return "", err
	}

	batchScriptFull := j.OutputScriptPth + "/scripts/" + j.ParentJobID + "_" + uniqueID + ".bash"
	batchScript, err := os.Create(batchScriptFull)
	if err != nil {
		return "", fmt.Errorf("Unable to create job script: %v", err)
	}
	if _, err := batchScript.WriteString(j.ScriptContents); err != nil {
		return "", fmt.Errorf("Unable to write to job script (%s): %v", batchScriptFull, err)
	}

	if err := os.Chmod(batchScriptFull, 0750); err != nil {
		return "", err
	}
	if err := os.Chown(batchScriptFull, j.UID, j.GID); err != nil {
		return "", err
	}
	batchScript.Close()

	return batchScriptFull, nil
}

func (j *Job) tailPipe(pipe io.ReadCloser) {
	scanner := bufio.NewScanner(pipe)
	for scanner.Scan() {
		j.PrintToParent(scanner.Text())
	}
}

func fileExists(filename string) bool {
	info, err := os.Stat(filename)
	if os.IsNotExist(err) {
		return false
	}
	return !info.IsDir()
}

func (j *Job) tailFile(fileName string, done chan bool) {
	// wait for the file to exist if it does not already
	sleepTime, _ := time.ParseDuration("30ms")
	for !fileExists(fileName) {
		time.Sleep(sleepTime)
	}

	// now tail it once it's been created
	w := watcher.New()

	w.SetMaxEvents(1)
	w.FilterOps(watcher.Write)

	file, ferr := os.Open(fileName)
	if ferr != nil {
		log.Fatal(ferr)
	}
	defer file.Close()

	go func() {
		defer w.Close()
		finished := false

		for {
			scanner := bufio.NewScanner(file)
			for scanner.Scan() {
				text := scanner.Text()
				j.PrintToParent(scanner.Text())

				// this line is written at the end of the submitted job script
				// to say that the output is done in a synchronous way
				if text == JobCompletionString {
					finished = true
				}
			}

			if finished {
				return
			}

			file.Seek(0, os.SEEK_CUR)

			select {
			case <-w.Event:
				continue
			case err := <-w.Error:
				log.WithFields(log.Fields{
					"filename": fileName,
					"error":    err,
				}).Fatal("watcher error")
				return
			case <-w.Closed:
				log.WithFields(log.Fields{
					"filename": fileName,
				}).Warn("watcher unexpected closed")
				finished = true
			case <-done:
				// caller can kill the tail routine by closing the done channel
				finished = true
			}
		}
	}()

	// Watch this folder for changes.
	if err := w.Add(fileName); err != nil {
		log.Fatalln(err)
	}

	// Start the watching process - it'll check for changes every 5s.
	if err := w.Start(time.Second * 5); err != nil {
		log.Fatalln(err)
	}

}

func (j *Job) mkTempFile(job *Job, template string) (out string, err error) {
	file, err := ioutil.TempFile(job.OutputScriptPth, template)
	if err != nil {
		return "", err
	}

	fileName := file.Name()

	if err = os.Chown(fileName, job.UID, job.GID); err != nil {
		log.WithFields(log.Fields{
			"filename": fileName,
			"uid":      job.UID,
			"gid":      job.GID,
			"error":    err,
		}).Error("os.Chown() failed")
		return "", err
	}

	return fileName, nil
}

// setUid generate a valid GoLang cmd account for runner's setuid requirements.
// Returns system error if encountered
func (j *Job) setUid(args []string) (cmd *exec.Cmd, err error) {
	cmd = exec.Command(args[0], args[1:]...)

	if os.Geteuid() == 0 {
		//Assign setUID information and env. vars
		cmd.SysProcAttr = &syscall.SysProcAttr{}
		cmd.SysProcAttr.Credential = &syscall.Credential{Uid: uint32(j.UID), Gid: uint32(j.GID)}
	}

	// Sanitize the environment
	user, err := user.LookupId(fmt.Sprintf("%d", j.UID))
	if err != nil {
		log.WithFields(log.Fields{
			"ParentJobID": j.ParentJobID,
			"UID":         j.UID,
		}).Debug("User lookup failed")
		return nil, err
	}

	var safeEnv []string
	for _, entry := range os.Environ() {
		env := strings.SplitN(entry, "=", 2)
		switch env[0] {
		case "LOGNAME", "USER":
			if user != nil { // Return the setuid username
				safeEnv = append(safeEnv, fmt.Sprintf("%s=%s", env[0], user.Username))
			}
		case "HOME":
			if user != nil { // Return the setuid user home directory
				safeEnv = append(safeEnv, fmt.Sprintf("%s=%s", env[0], user.HomeDir))
			}
		case "PATH":
			safeEnv = append(safeEnv, entry)
		default:
			// TODO: identify what is "safe"
			safeEnv = append(safeEnv, entry)
		}
	}
	cmd.Env = safeEnv

	log.WithFields(log.Fields{
		"args": args,
		"uid":  j.UID,
		"gid":  j.GID,
		"env":  safeEnv,
	}).Debug("Built SetUID command")

	return
}

// findScheduler checks for a userdefined batch system and if one
// is not defined then attempts to locate a valid one on the system's PATH
func (j *Job) findScheduler() (string, error) {
	if j.SpecifiedBatch != "" {
		return checkSupportSchedulers(j.SpecifiedBatch)
	}
	_, err := exec.LookPath("sbatch")
	if err == nil {
		return "slurm", nil
	}
	_, err = exec.LookPath("bsub")
	if err == nil {
		return "lsf", nil
	}
	_, err = exec.LookPath("qsub")
	if err == nil {
		_, err = exec.LookPath("qstat")
		if err == nil {
			return "cobalt", nil
		} else {
			return "", fmt.Errorf("Cobalt detected but can't monitor (found qsub but no qstat)")
		}
	}
	return "", fmt.Errorf("No batch system found")
}

// checkSupportSchedulers verifies that the user defined batch system is currently
// supported and return the appropriate internal system identify
func checkSupportSchedulers(sys string) (string, error) {
	supported := map[string]string{
		"slurm":  "slurm",
		"lsf":    "lsf",
		"cobalt": "cobalt",
	}
	if val, ok := supported[strings.ToLower(sys)]; ok {
		return val, nil
	}

	return "", fmt.Errorf("No supported batch system for: %s", sys)
}
