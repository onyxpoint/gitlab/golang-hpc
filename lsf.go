package hpc

import (
	"bufio"
	"errors"
	"fmt"
	"io"
	"os/exec"
	"regexp"
	"sync"

	log "github.com/sirupsen/logrus"
)

type LSFJob struct {
	*Job
	batchCommand string
	args         []string
	jobID        string
}

func (j LSFJob) New(job *Job) (LSFJob, error) {
	//Create script Check for Errors
	script, err := job.buildScript()
	if err != nil {
		log.WithFields(log.Fields{
			"ParentJobID": j.ParentJobID,
			"Error":       err,
		}).Debug("Failed to create job script")
		return LSFJob{}, err
	}

	//Assemble bash command
	execArgs := []string{"-I"}

	//Handle Native Specs
	if len(job.NativeSpecs) != 0 {
		//Defines an array of illegal arguments which will not be passed in as native specifications
		illegalArguments := []string{"-e", "-o", "-eo"}
		warnings := job.CheckIllegalParams(job.NativeSpecs, illegalArguments)
		for i := range warnings {
			job.PrintWarning(warnings[i])
		}
		execArgs = append(execArgs, job.NativeSpecs...)
	}

	execArgs = append(execArgs, script)

	return LSFJob{job, "bsub", execArgs, ""}, nil
}

func (j *LSFJob) RunJob() (builderr error, syserr error) {
	cmd, syserr := j.Job.setUid(append([]string{j.batchCommand}, j.args...))
	if syserr != nil {
		return
	}

	var stdout, stderr io.ReadCloser

	stdout, syserr = cmd.StdoutPipe()
	if syserr != nil {
		log.WithFields(log.Fields{
			"error": syserr,
		}).Warn("Error creating stdout pipe")
		j.Job.PrintToParent("Error creating stdout pipe")
		return
	}

	stderr, syserr = cmd.StderrPipe()
	if syserr != nil {
		log.WithFields(log.Fields{
			"error": syserr,
		}).Warn("Error creating stderr pipe")
		j.PrintToParent("Error creating stderr pipe")
		return
	}

	syserr = cmd.Start()
	if syserr != nil {
		log.WithFields(log.Fields{
			"args":  cmd.Args,
			"error": syserr,
		}).Warn("Failed to start command")
		j.PrintToParent("Failed to start command")
		return
	}

	// get job id from first line of LSF output
	jobid, syserr := j.getJobID(stdout)
	if syserr != nil {
		log.WithFields(log.Fields{
			"error": syserr,
		}).Warn("Couldn't get jobid from LSF")
		// TODO: identify desired behavior when a jobID cannot be obtained
	}
	j.jobID = jobid

	// read stdout and stderr in parallel and print them to the parent
	var wg sync.WaitGroup
	wg.Add(2)

	go func() {
		defer wg.Done()
		j.Job.tailPipe(stdout)
	}()

	go func() {
		defer wg.Done()
		j.Job.tailPipe(stderr)
	}()

	// force the tailPipe calls to complete first
	wg.Wait()

	// then wait on the command itself (this closes its pipes)
	err := cmd.Wait()
	if err != nil {
		if _, ok := err.(*exec.ExitError); ok {
			builderr = err
		} else {
			syserr = err
		}
		log.WithFields(log.Fields{
			"args":  cmd.Args,
			"error": err,
		}).Debug("Command failed")
		return
	}

	log.Debug("Job completed.")
	return
}

func (j *LSFJob) getJobID(pipe io.ReadCloser) (string, error) {
	jobid_re := regexp.MustCompile(`Job <(\d+)>`)

	scanner := bufio.NewScanner(pipe)
	scanner.Scan()
	line := scanner.Text()
	j.PrintToParent(line)

	matches := jobid_re.FindStringSubmatch(line)
	if len(matches) == 0 {
		return "", errors.New("Can't get JobID from first line of LSF output")
	}

	return matches[1], nil
}

// KillJob build command to kill job (scancel), j.jobID must be assigned.
// Returns system error if encountered.
func (j *LSFJob) KillJob() (err error) {
	cmd, err := j.Job.setUid([]string{"bkill", j.jobID})
	if err != nil {
		return err
	}
	ret, err := cmd.Output()
	if err != nil {
		return fmt.Errorf("Cannot kill job. %v", err)
	}
	log.WithFields(log.Fields{
		"ParentJobID": j.ParentJobID,
		"LSFJobID":    j.jobID,
		"Output":      string(ret),
	}).Debug("Job successfully cancled")
	return
}
