package hpc

import (
	"fmt"
	"io/ioutil"
	"os"
	"os/exec"
	"strings"
	"testing"
)

var tmpfile *os.File

// output script location.  The %d will be expanded to the current pid,
// so that iterative runs of this test have separate storage locations.
const outputScriptPath = "/g/g21/gamblin2/runner-out/%d"

// Length of the test output (we'll print this many numbers, one per line)
const outputLength = 100000

// this is our test submission script
var scriptContents = fmt.Sprintf(`
for i in $(seq 1 %d); do echo $i; done
`, outputLength)

// write to temp file for later validation.
func PrintOut(text string) {
	tmpfile.WriteString(text + "\n")
	fmt.Printf("OUT    %s\n", text)
}

// this isn't tested right now.
func PrintErr(text string) {
	fmt.Printf("ERR    %s\n", text)
}

func TestOutput(t *testing.T) {
	tmpfile, _ = ioutil.TempFile("", "TestOutput")

	// set the flags for the particular resource manager
	// SLURM is the default if we don't find anything else.
	nativeSpecs := []string{"-ppdebug", "-n", "1"}
	_, err := exec.LookPath("bsub")
	if err == nil {
		nativeSpecs = []string{"-q", "pbatch", "-nnodes", "1", "-G", "guests"}
	}

	j := &Job{
		ScriptContents:  scriptContents,
		NativeSpecs:     nativeSpecs,
		UID:             57095,
		GID:             57095,
		OutputScriptPth: fmt.Sprintf(outputScriptPath, os.Getpid()),
		BatchExecution:  true,
		PrintToParent:   PrintOut,
		PrintWarning:    PrintErr,
	}

	j.Run()

	tmpfile.Close()

	actualBytes, _ := ioutil.ReadFile(tmpfile.Name())
	actualOutput := string(actualBytes)

	var expectedOutput = "\n"
	for i := 1; i <= outputLength; i++ {
		expectedOutput += fmt.Sprintf("%d\n", i)
	}
	expectedOutput += JobCompletionString + "\n"

	// use HasSuffix to ignore anything printed from the user's shell
	// init files.
	if !strings.HasSuffix(actualOutput, expectedOutput) {
		t.Errorf(`expected:
` + expectedOutput + `
found:
` + actualOutput + "\n")
	}
}
