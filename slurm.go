package hpc

import (
	"fmt"
	"os"
	"path"
	"regexp"
	"strings"
	"sync"
	"time"

	log "github.com/sirupsen/logrus"
)

type SlurmJob struct {
	*Job
	batchCommand string
	args         []string
	out          []string
	statusCmd    string
	exitCodeCmd  string
	jobID        string
	sleepTime    time.Duration
}

func (j SlurmJob) New(job *Job) (SlurmJob, error) {
	//Create script Check for Errors
	script, err := job.buildScript()
	if err != nil {
		log.WithFields(log.Fields{
			"ParentJobID": j.ParentJobID,
			"Error":       err,
		}).Debug("Failed to create job script")
		return SlurmJob{}, err
	}

	//Get output script paths -- initially, use %j to have SLURM substitute the
	// job id, then fix it later once we know what the job id is
	outputScriptPath := path.Join(job.OutputScriptPth, "slurm-out-%j.log")
	execArgs := []string{"-o", outputScriptPath}

	//Handle Native Specs
	if len(job.NativeSpecs) != 0 {
		//Defines an array of illegal arguments which will not be passed in as native specifications
		illegalArguments := []string{"-o", "--output"}
		warnings := j.Job.CheckIllegalParams(job.NativeSpecs, illegalArguments)
		for i := range warnings {
			job.PrintWarning(warnings[i])
		}
		execArgs = append(execArgs, job.NativeSpecs...)
	}

	files := []string{outputScriptPath}
	execArgs = append(execArgs, script)

	return SlurmJob{job, "sbatch", execArgs, files, "squeue", "sacct", "", 30 * time.Second}, nil
}

func (j *SlurmJob) RunJob() (builderr error, syserr error) {
	// execute the sbatch submit command
	cmd, err := j.Job.setUid(append([]string{j.batchCommand}, j.args...))
	if err != nil {
		return nil, err
	}
	stdOut, err := cmd.CombinedOutput()
	if err != nil {
		return nil, fmt.Errorf(string(stdOut))
	}

	j.PrintToParent(string(stdOut))
	j.jobID, syserr = sbatchJobID(string(stdOut))
	if syserr != nil {
		return
	}

	// fix up %j's in the filenames in j.out (%% is the escape for %)
	fixup_job := regexp.MustCompile(`(^|[^%])%j`)
	for i, file := range j.out {
		j.out[i] = fixup_job.ReplaceAllString(file, "${1}"+j.jobID)
	}

	//Build a command to check job status
	statusCmd, _ := j.Job.setUid([]string{j.statusCmd, "--job", j.jobID})
	done := make(chan bool)

	// We'll wait for ALL the files to finish, OR squeue to report the job done
	var wg sync.WaitGroup
	wg.Add(len(j.out))

	// Tail the job's output until we find JobCompletionString, which
	// tells us we're done.
	//
	// If the job terminates abnormally, we MAY not see
	// JobCompletionString in the output file, so tailFile can also be
	// killed by closing the `done` channel -- see below.
	for _, file := range j.out {
		log.WithFields(log.Fields{
			"file": file,
		}).Debug("Watching file")
		go func() {
			defer wg.Done()
			j.Job.tailFile(file, done)
		}()
		defer os.Remove(file)
	}

	// make a channel that will be closed when all files are done.
	files_done := make(chan bool)
	go func() {
		wg.Wait()
		close(files_done)
	}()

	status := *statusCmd
	ret, err := status.Output()

	// In case the job terminates abnormally, we also monitor that the
	// job is still known to the batch system using squeue.  If it shows
	// up as NOT completed, we'll bail on tailFile because it may never
	// receive JobCompletionString.  We give it some time(NFSTimeout) to
	// finish reading before we kill it this way.
	squeue_done := make(chan bool)
	go func() {
		//Loop until the job id is no longer in squeue output.
		for strings.Contains(string(ret), fmt.Sprint(j.jobID)) {
			time.Sleep(j.sleepTime)

			status = *statusCmd
			log.WithFields(log.Fields{
				"args": status.Args,
			}).Debug("Checking job")
			ret, err = status.Output()
		}

		// Give tailFile some time to finish reading data from NFS before
		// we kill it due to squeue reporting that the job is done.
		sleepTime, _ := time.ParseDuration(NFSTimeout)
		time.Sleep(sleepTime)

		close(squeue_done)
	}()

	// wait for one of the above channels to close and signal we're done
	log.Debug("Waiting for output.")
	select {
	case <-files_done:
		log.Debug("All files finished writing.")
	case <-squeue_done:
		log.Debug("Job done but files not complete. Stopping due to timeout.")
	}

	// this kills tailFile if squeue checks finished first.
	close(done)

	log.WithFields(log.Fields{
		"jobid": j.jobID,
	}).Debug("Job completed.")

	return j.obtainSacct(cmd.Args)
}

// KillJob build command to kill job (scancel), j.jobID must be assigned.
// Returns system error if encountered.
func (j *SlurmJob) KillJob() (err error) {
	cmd, err := j.Job.setUid([]string{"scancel", j.jobID})
	if err != nil {
		return err
	}
	ret, err := cmd.Output()
	if err != nil {
		return fmt.Errorf("Cannot kill job. %v", err)
	}
	log.WithFields(log.Fields{
		"ParentJobID": j.ParentJobID,
		"SlurmJobID":  j.jobID,
		"Output":      string(ret),
	}).Debug("Job successfully cancled")
	return
}

// sbatchJobID parses stdout patterns from sbatch call to retrieve jobid
// returns system error if encountered
func sbatchJobID(out string) (string, error) {
	a := regexp.MustCompile(`[S-s]ubmitted\sbatch\sjob\s(\d+)`)
	matches := a.FindStringSubmatch(out)
	if len(matches) != 2 {
		return "", fmt.Errorf("Unable to obtain jobID from sbatch stdout.")
	}
	return matches[1], nil
}

// sacctState parses stdout from the sacct cmd to retrieve the job's state
// expected: $ sacct -n -j # --format=state
func sacctState(out string) string {
	lines := strings.Split(out, "\n")
	return strings.TrimSpace(lines[0])
}

// ObtainSacct uses Slurm's sacct program to obtain the accouting information for the job
// returns build or system error. System error is only encountered if sacct cannot be invoked
func (j *SlurmJob) obtainSacct(jobargs []string) (builderr error, syserr error) {
	// Build a command to get exit status of the Job, executed as setuid target
	sacctCmd, _ := j.Job.setUid([]string{j.exitCodeCmd, "-n", "-j", j.jobID, "--format=state"})

	account := *sacctCmd
	ret, err := account.Output()

	// Wait for job to complete, then get return code
	for {
		if err != nil || string(ret) == "" {
			log.WithFields(log.Fields{
				"args":  sacctCmd.Args,
				"error": err,
			}).Debug("System Error: Cannot get output via sacct")
			return nil, fmt.Errorf("System Error: Cannot get output via sacct: %v", err)
		}

		jobState := sacctState(string(ret))

		// if the job still shows that it's running, wait for sacct to
		// show it's completed before getting the return code.
		if jobState == "PENDING" || jobState == "RUNNING" {
			log.WithFields(log.Fields{
				"jobState": jobState,
				"jobID":    j.jobID,
			}).Debug("Waiting for job to complete before getting return code.")
			account = *sacctCmd
			ret, err = account.Output()
			time.Sleep(j.sleepTime)
			continue
		}

		var msg string
		if jobState == "COMPLETED" {
			msg = "Job successful"
		} else {
			msg = fmt.Sprintf("Final job state: %v", jobState)
			builderr = fmt.Errorf(msg)
		}

		log.WithFields(log.Fields{
			"sacct":    sacctCmd.Args,
			"error":    err,
			"jobID":    j.jobID,
			"jobState": jobState,
		}).Debug(msg)
		break
	}
	return
}
