package hpc

import (
	"bufio"
	"bytes"
	"fmt"
	"io"
	"os/exec"
)

func (j *Job) New(job *Job) (*Job, error) {
	return job, nil
}

// RunJob executes a "no-batch" script on the host shell, bypassing the
// underlying scheduler. Return build, system error if encountered
func (j *Job) RunJob() (error, error) {
	cmd, err := j.setUid([]string{"bash", "--login"})
	if err != nil {
		return nil, err
	}
	cmd.Stdin = bytes.NewBufferString(j.ScriptContents)

	stdout, err := cmd.StdoutPipe()
	if err != nil {
		return nil, fmt.Errorf("Failed to create StdoutPipe for command: %s", err)
	}
	stderr, err := cmd.StderrPipe()
	if err != nil {
		return nil, fmt.Errorf("Failed to create StderrPipe for command: %s", err)
	}

	go func() {
		reader := io.MultiReader(stderr, stdout)
		scanner := bufio.NewScanner(reader)
		for scanner.Scan() {
			msg := scanner.Text()
			j.PrintToParent(fmt.Sprintf("%v", msg))
		}
	}()

	if err := cmd.Start(); err != nil {
		return nil, fmt.Errorf("Failed to start process: %s", err)
	}

	if err := cmd.Wait(); err != nil {
		if _, ok := err.(*exec.ExitError); ok {
			j.PrintWarning(fmt.Sprintf("%v\n", err))
			return err, nil
		} else {
			return nil, err
		}
	}

	return nil, nil
}
