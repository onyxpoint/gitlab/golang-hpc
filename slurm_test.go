package hpc

import (
	"testing"
)

func Test_sacctState(t *testing.T) {
	type args struct {
		out string
	}
	tests := []struct {
		name string
		args args
		want string
	}{
		{name: "Return",
			args: args{
				out: `   TIMEOUT
 	CANCELLED
`,
			},
			want: "TIMEOUT",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := sacctState(tt.args.out); got != tt.want {
				t.Errorf("sacctState() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_sbatchJobID(t *testing.T) {
	type args struct {
		out string
	}
	tests := []struct {
		name    string
		args    args
		want    string
		wantErr bool
	}{
		{
			name:    "Standard return",
			args:    args{out: "Submitted batch job 123\n"},
			want:    "123",
			wantErr: false,
		},
		{
			name:    "Cluster specific return",
			args:    args{out: "Submitted batch job 456789 on cluster example"},
			want:    "456789",
			wantErr: false,
		}, {
			name:    "Debug output",
			args:    args{out: "sbatch: slurm_spank_init nnodes=0\nsbatch: argc[0] is host=localhost\nsbatch: argc[1] is test=true\nSubmitted batch job 5\nsbatch: slurm_spank_exit"},
			want:    "5",
			wantErr: false,
		}, {
			name:    "No jobID",
			args:    args{out: "Submitted batch job"},
			want:    "",
			wantErr: true,
		}, {
			name:    "Multiple jobID found",
			args:    args{out: "TEST\nSubmitted batch job 1\nSubmitted batch job 2"},
			want:    "1",
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := sbatchJobID(tt.args.out)
			if (err != nil) != tt.wantErr {
				t.Errorf("sbatchJobID() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("sbatchJobID() = %v, want %v", got, tt.want)
			}
		})
	}
}
