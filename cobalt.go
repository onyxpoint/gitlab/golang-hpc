package hpc

import (
	"bytes"
	"fmt"
	"os"
	"strconv"
	"strings"
	"sync"
	"time"

	log "github.com/sirupsen/logrus"
)

type CobaltJob struct {
	*Job
	batchCommand string
	args         []string
	out          []string
	statusCmd    string
	jobID        string
	sleepTime    time.Duration
}

func (j CobaltJob) New(job *Job) (CobaltJob, error) {
	//Create script Check for Errors
	script, err := job.buildScript()
	if err != nil {
		log.WithFields(log.Fields{
			"ParentJobID": j.ParentJobID,
			"Error":       err,
		}).Debug("Failed to create job script")
		return CobaltJob{}, err
	}

	//Get output script paths
	var outputScriptPath, errorScriptPath, logScriptPath string

	outputScriptPath, err = job.mkTempFile(job, "cobalt_out-*.log")
	if err != nil {
		log.Warn("Failed to create output")
		job.PrintToParent("Failed to create output")
		return CobaltJob{}, err
	}

	errorScriptPath, err = job.mkTempFile(job, "cobalt_err-*.log")
	if err != nil {
		log.Warn("Failed to create error output")
		job.PrintToParent("Failed to create error output")
		return CobaltJob{}, err
	}

	logScriptPath, err = job.mkTempFile(job, "cobalt_debug-*.log")
	if err != nil {
		log.Warn("Failed to create debug output")
		job.PrintToParent("Failed to create debug output")
		return CobaltJob{}, err
	}

	files := []string{outputScriptPath, errorScriptPath, logScriptPath}
	execArgs := []string{"-o", outputScriptPath, "-e", errorScriptPath, "--debuglog", logScriptPath}

	//Handle Native Specs
	if len(job.NativeSpecs) != 0 {
		//Defines an array of illegal arguments which will not be passed in as native specifications
		illegalArguments := []string{"-o", "-e", "--debuglog", " "}
		warnings := j.Job.CheckIllegalParams(job.NativeSpecs, illegalArguments)
		for i := range warnings {
			job.PrintWarning(warnings[i])
		}
		execArgs = append(execArgs, job.NativeSpecs...)
	}

	execArgs = append(execArgs, script)

	return CobaltJob{job, "qsub", execArgs, files, "qstat", "", 30 * time.Second}, nil
}

func (j *CobaltJob) RunJob() (builderr error, syserr error) {
	cmd, syserr := j.Job.setUid(append([]string{j.batchCommand}, j.args...))
	if syserr != nil {
		return
	}

	//Handle stdout and stderr
	var stdout, stderr bytes.Buffer
	cmd.Stdout = &stdout
	cmd.Stderr = &stderr
	//Run the command, check for errors
	syserr = cmd.Run()

	errStr := stderr.String()
	if errStr != "" {
		log.Warn(errStr)
		j.PrintToParent(errStr)
	}

	if syserr != nil {
		log.WithFields(log.Fields{
			"args": cmd.Args,
		}).Warn("Command failed.")
		j.PrintToParent(fmt.Sprintf("Command '%s' failed.", strings.Join(cmd.Args, " ")))
		return
	}

	jobid, syserr := strconv.Atoi(strings.TrimSpace(stdout.String()))
	if syserr != nil {
		log.WithFields(log.Fields{
			"error": syserr,
		}).Warn("Failed to read job ID")
		j.PrintToParent(fmt.Sprintf("Failed to read job ID: %#v", syserr))
		return
	}
	j.jobID = strconv.Itoa(jobid)

	log.WithFields(log.Fields{
		"jobid": jobid,
	}).Info("Waiting for job to complete.")
	j.PrintToParent(fmt.Sprintf("Waiting for job %d to complete.", jobid))

	//Build a command to check job status
	statusCmd, _ := j.Job.setUid([]string{j.statusCmd, fmt.Sprintf("%d", jobid)})

	var wg sync.WaitGroup
	wg.Add(len(j.out))

	done := make(chan bool)
	for _, file := range j.out {
		log.WithFields(log.Fields{
			"file": file,
		}).Debug("Watching file")
		go func() {
			defer wg.Done()
			j.Job.tailFile(file, done)
		}()
		defer os.Remove(file)
	}

	status := *statusCmd
	ret := status.Run()

	//Loop until qstat returns non-zero
	for ret == nil {
		time.Sleep(j.sleepTime)

		status = *statusCmd
		log.WithFields(log.Fields{
			"args": status.Args,
		}).Debug("Checking job")
		ret = status.Run()
	}

	log.WithFields(log.Fields{
		"jobid": jobid,
	}).Debug("Job completed.  Waiting for output.")

	time.Sleep(j.sleepTime)
	close(done)
	wg.Wait()
	return
}

// KillJob build command to kill job (qdel), j.jobID must be assigned.
// Returns system error if encountered.
func (j *CobaltJob) KillJob() (err error) {
	cmd, err := j.Job.setUid([]string{"qdel", j.jobID})
	if err != nil {
		return err
	}
	ret, err := cmd.Output()
	if err != nil {
		return fmt.Errorf("Cannot kill job. %v", err)
	}
	log.WithFields(log.Fields{
		"ParentJobID": j.ParentJobID,
		"CobaltJobID": j.jobID,
		"Output":      string(ret),
	}).Debug("Job successfully cancled")
	return
}
